use lorelei::routes;
use std::fs;
use std::net::IpAddr;
use std::path::PathBuf;
use std::str::FromStr;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(
    name = "lorelei",
    about = "Perform predefined actions receiving HTTP requests signals.",
    after_help = concat!(
        "EXAMPLES:\n",
        "   lorelei --port 43819 --actions-dir ./lorelei-actions",
    ),
)]

struct Cli {
    /// The port to listen in.
    #[structopt(long)]
    port: i32,
    #[structopt(parse(from_os_str), long)]
    actions_dir: PathBuf,
    #[structopt(parse(from_os_str), long)]
    working_dir: Option<PathBuf>,
}

/// Verifies the actions and working directories exist and are directories.
fn verify_directories(actions_dir: &PathBuf, working_dir: &PathBuf) {
    if !actions_dir.exists() {
        panic!("The specified actions directory does not exist.");
    }
    if !actions_dir.is_dir() {
        panic!("The specified actions directory is not a directory.");
    }
    if !working_dir.exists() {
        panic!("The specified working directory does not exist.");
    }
    if !working_dir.is_dir() {
        panic!("The specified working directory is not a directory.");
    }
}

#[tokio::main]
async fn main() {
    let args: Cli = Cli::from_args();
    let actions_dir = args.actions_dir;
    let working_dir = args.working_dir.unwrap_or_else(|| actions_dir.clone());
    if !working_dir.exists() {
        fs::create_dir_all(&working_dir).expect("Could not create working directory.");
    }
    verify_directories(&actions_dir, &working_dir);

    let state = routes::State {
        actions_dir,
        working_dir,
    };
    let addr = IpAddr::from_str("::0").unwrap();
    println!("Listening on address '{}' on port '{}'", addr, args.port);
    routes::serve(state, (addr, args.port as u16)).await;
}
