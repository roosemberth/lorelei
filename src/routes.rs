use crate::actions::{trigger_action, TriggerActionResult};
use std::net::SocketAddr;
use std::path::PathBuf;
use warp::Filter;

/// The configuration state shared across route handlers.
pub struct State {
    pub actions_dir: PathBuf,
    pub working_dir: PathBuf,
}

async fn create_action(
    state: std::sync::Arc<State>,
    action: String,
) -> Result<impl warp::Reply, warp::Rejection> {
    match trigger_action(&action, &state.actions_dir, &state.working_dir) {
        TriggerActionResult::UnknownAction => Ok(warp::http::Response::builder()
            .status(warp::http::StatusCode::NOT_FOUND)
            .body("")
            .unwrap()),
        TriggerActionResult::ActionAlreadyRunning => Ok(warp::http::Response::builder()
            .status(warp::http::StatusCode::CONFLICT)
            .body("")
            .unwrap()),
        TriggerActionResult::ActionTriggered(id) => Ok(warp::http::Response::builder()
            .status(warp::http::StatusCode::CREATED)
            .header("Location", format!("/actions/{}", id))
            .body("")
            .unwrap()),
    }
}

pub async fn serve(state: State, addr: impl Into<SocketAddr>) {
    let state = std::sync::Arc::new(state);
    let with_state = warp::any().map(move || state.clone());
    let new_action = with_state
        .clone()
        .and(warp::post())
        .and(warp::path!(String))
        .and(warp::path::end())
        .and_then(create_action);
    let actions = warp::path("actions").and(new_action);
    warp::serve(actions).run(addr).await;
}
