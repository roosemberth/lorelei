use std::path::PathBuf;
use std::{fs, io};
use uuid::Uuid;

pub enum TriggerActionResult {
    UnknownAction,
    ActionAlreadyRunning,
    ActionTriggered(Uuid),
}

/// Returns whether this action is currently being executed by checking whether
/// the action working directory contains at least two entries with extensions
/// `.stdout` or `.stderr`.
pub fn action_seems_to_be_running(action_working_dir: &PathBuf) -> bool {
    let failed_read_str = format!(
        "Could not read action working directory '{}'",
        action_working_dir.display()
    );

    let number_of_matching_files = fs::read_dir(action_working_dir)
        .expect(&failed_read_str)
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<PathBuf>, io::Error>>()
        .expect(&failed_read_str)
        .iter_mut()
        .filter(|e| match e.extension().and_then(|e| e.to_str()) {
            Some("stdout") => true,
            Some("stderr") => true,
            _ => false,
        })
        .count();
    number_of_matching_files > 1
}

pub fn create_action_output_files(action_working_dir: &PathBuf) -> Uuid {
    let failed_create_str = format!(
        "Could not create action output files on '{}'",
        action_working_dir.display()
    );
    let id = Uuid::new_v4();
    let stdout_file = action_working_dir.join(format!("{}.stdout", id));
    std::fs::File::create(&stdout_file).expect(&failed_create_str);
    let stderr_file = action_working_dir.join(format!("{}.stderr", id));
    std::fs::File::create(&stderr_file).expect(&failed_create_str);
    id
}

pub fn remove_action_output_files(action_working_dir: &PathBuf, id: &Uuid) -> std::io::Result<()> {
    let stdout_file = action_working_dir.join(format!("{}.stdout", id));
    let stderr_file = action_working_dir.join(format!("{}.stderr", id));
    std::fs::remove_file(stdout_file).and_then(|_| std::fs::remove_file(stderr_file))
}

/// Triggers the specified action.
pub fn trigger_action(
    action: &str,
    actions_dir: &PathBuf,
    working_dir: &PathBuf,
) -> TriggerActionResult {
    if !actions_dir.join(action).exists() {
        return TriggerActionResult::UnknownAction;
    }
    let action_working_dir = working_dir.join(action);
    if !action_working_dir.exists() {
        fs::create_dir_all(&action_working_dir).expect(&format!(
            "Could not create action '{}' working directory.",
            action
        ));
    }
    if action_seems_to_be_running(&action_working_dir) {
        println!(
            "Request to trigger action '{}', but such action seems to be \
            already running.",
            action
        );
        return TriggerActionResult::ActionAlreadyRunning;
    }

    let id = create_action_output_files(&action_working_dir);
    println!(
        "I have successfully created two output files in {}. \
         I should be executing the specified action, but this is not yet \
         implemented. I will spawn a thread to remove the files in 10 seconds.",
        action_working_dir.display()
    );
    {
        let id = id.clone();
        let action_working_dir = action_working_dir.clone();
        tokio::task::spawn(async move {
            tokio::time::sleep(std::time::Duration::new(10, 0)).await;
            println!(
                "I am removing the output files in {} since the 10 seconds elapsed.",
                action_working_dir.display()
            );
            remove_action_output_files(&action_working_dir, &id).expect(&format!(
                "I failed to remove the output files in {}.",
                action_working_dir.display()
            ));
        });
    }
    TriggerActionResult::ActionTriggered(id)
}
