# Lorelei

Lorelei is a small and simple HTTP daemon to perform predefined shell commands
(_actions_) upon receiving a request.

Lorelei provides the following characteristics:

- Ticketing: Triggering actions yields a ticket that may be used to query the
  state of the action.
- Isolation: Actions may not be executed concurrently.

One of the main use-cases for Lorelei is to be a simple deployment system
when the system being deployed to is not in the same trust zone as the
continuous integration where the software is developed.
Thus, Lorelei can be used to reduce the attack surface by providing actions to
download and deploy the latest release artifact from the CI.

## Design guidelines

- Lorelei uses the filesystem to store its state.
- Lorelei takes a directory containing a tree of _actions_, this directory may
  be read-only or read-write.
  An action is a directory containing an executable (script or binary) with the
  same name.
- Lorelei requires a working directory. Use of a tmpfs is recommended.
  If no directory is specified, the actions directory will be used as working
  directory.
- Actions are executed from within the _action working directory_.
  This directory is located inside Lorelei's working directory and is named as
  the action.
  It is created if it does not exist.
- Two files will be created in the action working directory prior
  to executing the action.
  They will be named `${ticket}.stdout` and `${ticket}.stderr` and be used to
  communicate `stdout` and `stderr` to Lorelei.
  In the event Lorelei stops before some action, these will allow the action to
  continue their execution and their outputs be retrieved by manual intervention
  if needed.
  The named pipes will be removed after the action has finished execution.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
